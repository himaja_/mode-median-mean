import java.util.*;

public class Statistics {

    public static double getMean(int[] arr){

        if(arr.length==0)
            return 0;

        int sum=0;
        for(int i:arr)
            sum+=i;

        return (double)sum/arr.length;

    }

    public static double getMedian(int[] arr){

        Arrays.sort(arr);
        int size= arr.length;
        double median;
        if(size%2 != 0)
            median= arr[size/2];
        else
            median= (arr[(size-1)/2] + arr[size/2]) / 2.0;

        return median;

    }

    public static double getMode(int[] arr){

        int maximum_element= Integer.MIN_VALUE;
        for(int i:arr){
            if(maximum_element<i)
                maximum_element= i;
        }

        int[] count_array= new int[maximum_element+1];
        Arrays.fill(count_array, 0);
        for(int i:arr){
            count_array[i]+=1;
        }

        double mode=Integer.MIN_VALUE, maximum_value= Integer.MIN_VALUE;
        for(int i=0; i< count_array.length; i++){
            if(maximum_value<count_array[i]){
                maximum_value= count_array[i];
                mode= i;
            }
        }

        return mode;
        
    }

    public static void main(String[] args) {

        Scanner sc= new Scanner(System.in);

        int size;
        size= sc.nextInt();

        int[] arr= new int[size];
        for(int i=0; i<size; i++)
            arr[i]= sc.nextInt();

        double mean, median, mode;
        mean= getMean(arr);
        median= getMedian(arr);
        mode= getMode(arr);

        System.out.printf("%.2f\n%.2f\n%.2f", mean, median, mode);

    }

}
